/*!
 *  @author    Alessio Piucci
 *  @brief     Library to handle the geometry simulation
 */

#include "TFRGeometry.h"

//------------
// Constructor
//------------
TFRGeometry::TFRGeometry(const std::string configFile_name){

  //-----------------------------------------------//
  //  set the geometry from the input config file  //
  //-----------------------------------------------//
  
  SetGeometry(configFile_name);
  
  //----------------------------------//
  //  set the layer id --> layer map  //
  //----------------------------------//
  
  //loop over the detector layers, to create the keys of the map (i.e. indexing by layer id)
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_layer((TFRLayers*) this->GetLayers());
  TFRLayer *curr_layer;
  
  //loop over the detector layers
  while((curr_layer = (TFRLayer*) it_layer.Next()))
    layer_map.insert(std::make_pair(curr_layer->GetID(), curr_layer));
  
  //--------------------------//
  //  set the magnetic field  //
  //--------------------------//
  
  //open the input config file
  boost::property_tree::ptree configFile;
  boost::property_tree::read_info(configFile_name, configFile);

  B_zmin = configFile.get<double>("B_field.z_min");
  B_zmax = configFile.get<double>("B_field.z_max");
  B_mag = configFile.get<double>("B_field.magnitude");


  //-------------------------------//
  //  set the multiple scattering  //
  //-------------------------------//

  mult_scatt = new TFRMultipleScattering();
  
  if(configFile.get<unsigned int>("multiple_scattering") == 1)
    multiple_scattering = true;
  else
    multiple_scattering = false;
  
}

//----------
// Destructor
//-----------
TFRGeometry::~TFRGeometry(){
  
}

//----------
// Set the detector geometry
//-----------
void TFRGeometry::SetGeometry(const std::string configFile_name){

  layers = new TFRLayers();
  
  //open the input config file
  boost::property_tree::ptree configFile;
  boost::property_tree::read_info(configFile_name, configFile);

  //temp list of layers, that I will sort by z positions
  std::list<TFRLayer*> layer_list;
  
  //loop over layers, and add them to the detector
  for(boost::property_tree::ptree::const_iterator config_layer = configFile.get_child("layers").begin();
      config_layer != configFile.get_child("layers").end(); ++config_layer){

    //layer position
    TEveVectorD layer_position(config_layer->second.get<double>("position.x"),
                               config_layer->second.get<double>("position.y"),
                               config_layer->second.get<double>("position.z"));
    
    TFRLayer *curr_layer = new TFRLayer(0, layer_position,
                                        config_layer->second.get<double>("Euler_angles.phi") * (M_PI / 180.),
                                        config_layer->second.get<double>("Euler_angles.theta") * (M_PI / 180.),
                                        config_layer->second.get<double>("Euler_angles.psi") * (M_PI / 180.),
                                        config_layer->second.get<double>("x_size"),
                                        config_layer->second.get<double>("y_size"),
                                        config_layer->second.get<double>("dxdy"),
                                        config_layer->second.get<std::string>("sensor"),
                                        config_layer->second.get<double>("hit_res"),
                                        config_layer->second.get<double>("hit_eff"),
                                        config_layer->second.get<double>("noise_prob"),
                                        config_layer->second.get<double>("segm_x"),
                                        config_layer->second.get<double>("segm_y"),
					config_layer->second.get<double>("x_over_Xzero"));

    layer_list.push_back(curr_layer);
  }

  //sort the layers by z positions
  layer_list.sort(compare_z);

  //add the sorted layers to the layer array stored in the geometry,
  //also setting the layer ID
  for(std::list<TFRLayer*>::iterator it_layer = layer_list.begin();
      it_layer != layer_list.end(); ++it_layer){
    (*it_layer)->SetID(std::distance(layer_list.begin(), it_layer));
    layers->Add(*it_layer);
  }
  
  //set the geometry tag
  SetTag(configFile.get<std::string>("tag"));
  
  return;
}

//----------
// Clear the detector geometry
//----------
void TFRGeometry::ClearGeometry(){

  //unset the tag
  SetTag("");
  
  //clear the vector of layers
  layers->Clear();
  
  return;
}

//---------- 
// Dump the detector geometry to a output stream
//---------- 
void TFRGeometry::DumpGeometry(std::ostream out_stream){
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_layer((TFRLayers*) this->GetLayers());
  TFRLayer *curr_layer;

  //loop over detector layers
  while((curr_layer = (TFRLayer*) it_layer.Next())){
    
    out_stream << "layer_id\t" << curr_layer->GetID() << "\n";
    out_stream << "center_position\t" << curr_layer->GetPosition()[0]
	       << "\t" << curr_layer->GetPosition()[1]
	       << "\t" << curr_layer->GetPosition()[2] << "\n";
    /*
    out_stream << "x_max\t" << curr_layer->x_max << "\n";
    out_stream << "x_min\t" << curr_layer->x_min << "\n";
    out_stream << "y_max\t" << curr_layer->y_max << "\n";
    out_stream << "y_min\t" << curr_layer->y_min << "\n";
    out_stream << "phi\t" << (curr_layer->phi * 180.)/M_PI << "\n";
    out_stream << "theta\t" << (curr_layer->theta * 180.)/M_PI << "\n";
    out_stream << "psi\t" << (curr_layer->psi * 180.)/M_PI << "\n";
    */
    out_stream << "-----------------\n";
    
  }  //loop over the layers
  
  return;
}

//----------
// Dump the detector geometry to the std::cout stream
//----------
void TFRGeometry::DumpGeometry(){
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_layer((TFRLayers*) this->GetLayers());
  TFRLayer *curr_layer;

  //loop over detector layers
  while((curr_layer = (TFRLayer*) it_layer.Next())){
    
    std::cout << "layer_id\t" << curr_layer->GetID() << "\n";
    std::cout << "center_position\t" << curr_layer->GetPosition()[0]
	      << "\t" << curr_layer->GetPosition()[1]
	      << "\t" << curr_layer->GetPosition()[2] << "\n" << std::endl;
    std::cout << "-----------------\n" << std::endl;

  }  //loop over the layers                                                                                                                                                                            

  return;
}

//----------
//  Retrieve the layer, by its index
//----------
TFRLayer* TFRGeometry::GetLayer(const unsigned int layer_ID){

  if(layer_map.find(layer_ID) != layer_map.end())
    return (*layer_map.find(layer_ID)).second;
  else{
    std::cout << "Geometry::GetLayer: warning, layer " << layer_ID << "not found." << std::endl;
    return NULL;
  }
}

//----------
//  Returns the layers between z_1 and z_2 (z_min<=z_layer<zmax) ordered from z_1 to z_2*/
//----------
TFRLayers TFRGeometry::GetLayersBetween(const double z_1, const double z_2){

  TFRLayers layer_list;

  //I've no clue why the hell in ROOT there aren't nice implementations of iterators
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_layer((TFRLayers*) this->GetLayers());
 
  //determine min and max z
  double z_min=min(z_1,z_2);
  double z_max=max(z_1,z_2);

  if(z_1<z_2){
    for(int i = 0; i< this->GetLayers()->GetEntries(); i++){
      //check the z_min
      if(z_min > ((TFRLayer*)this->GetLayers()->At(i))->GetPosition()[2])
        continue;

      //if I'm above z_max, interrumpt (layers are ordered in z...)
      if(z_max <= ((TFRLayer*)this->GetLayers()->At(i))->GetPosition()[2])
        break;

      layer_list.Add(((TFRLayer*)this->GetLayers()->At(i)));
    
    }  //loop over the layers
  }
  else{
    for(int i = this->GetLayers()->GetEntries()-1; i>=0; i--){
      //check the z_max
      if(z_max <= ((TFRLayer*)this->GetLayers()->At(i))->GetPosition()[2])
        continue;

      //if I'm below z_min, interrumpt (layers are ordered in z...)
      if(z_min > ((TFRLayer*)this->GetLayers()->At(i))->GetPosition()[2])
        break;

      layer_list.Add(((TFRLayer*)this->GetLayers()->At(i)));
    
    }  //loop over the layers
  }
  
  return layer_list;
}

